# Kombiner
## A tool for cleaning, formatting and importing data from one or more sources

Kombiner is a tool to ease the process of cleaning, formatting and importing data to a production dataset from one or more data sources.

This tool was originally developed by [Robin McFee](https://github.com/rdmcfee) at [Clinicbook](http://www.clinicbook.com) for importing clinic and pracitioner listings.