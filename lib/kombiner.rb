require 'kombiner/version'
require 'typhoeus'

module Kombiner
  # Your code goes here...
  module Utils
  end

  class InvalidRecordError < StandardError; end
  class FinishedProcessingError < StandardError; end
end

Dir[File.dirname(__FILE__) + '/lib/*.rb'].each {|file| require file }
# Typhoeus::Config.verbose = true

# require 'kombiner/mapper'
# require 'kombiner/mixin'
# require 'kombiner/kombination'
# require 'kombiner/collector'
# require 'kombiner/extractor'
# require 'kombiner/data_target'
