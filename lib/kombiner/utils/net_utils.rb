require 'nokogiri'
# require 'httparty'
# require 'curb'
require 'typhoeus'

module Kombiner
  module Utils::NetUtils
    DEFAULT_REQUEST_FORMAT = :json
    # include HTTParty

    class RejectionError < StandardError; end
    
    DEFAULT_RETRYABLE_EXCEPTIONS = [Errno::ENOENT, Timeout::Error]
    # RESCUABLE_EXCEPTIONS = [UnsupportedFormat, UnsupportedURIScheme, ResponseError, RedirectionTooDeep, Errno::ENOENT]
    RESCUABLE_EXCEPTIONS = [Errno::ENOENT]

    def self.included(base)
      # inclue httparty on the base
      # base.send(:include, HTTParty)
      # base.send(:include, Curl)
      # base.send(:format, base::REQUEST_FORMAT)
    end

    def retryable(options={})
      opts = { :tries => 1, :on => DEFAULT_RETRYABLE_EXCEPTIONS }.merge(options)

      retry_exception, retries = opts[:on], opts[:tries]

      begin
        return yield
      rescue *retry_exception
        if (retries -= 1) > 0
          sleep 2
          retry 
        else
          raise
        end
      end
    end

    def execute(request_hash, include_response = false)
      # so that default vals are hashes
      request_hash = Hash.new({}).merge(request_hash)
      # FIXME handle follow redirects param properly.
      begin
        retryable(:tries => 2, :on => [Errno::ENOENT, Timeout::Error]) do
          req = Typhoeus::Request.new(request_hash[:url], {method: request_hash[:method], headers: request_hash[:headers], body: request_hash[:body]})
          # omg remove these horrible default headers.
          req.options[:headers].delete("User-Agent")
          req.options[:headers].delete("Expect")
          # set content length header if body present
          if request_hash[:body]
            req.options[:headers]['Content-Length'] = req.encoded_body.length
          end
          return handle_response(req.run, request_hash[:format], include_response)
        end
      rescue *RESCUABLE_EXCEPTIONS => e
        handle_e(e)
      end
    end

    def parse_cookie(resp)
      cookie_hash = CookieHash.new
      resp.get_fields('Set-Cookie').each { |c| cookie_hash.add_cookies(c) }
      cookie_hash
    end
  
    private

    def handle_response(response, format = self.class::REQUEST_FORMAT, include_response = false)
      if (400..600).include?(response.code)
        raise RejectionError.new("Bad response code: #{response.code} from request #{response.request.inspect}")
      end
      parse_body(response, include_response, format: format)
    end

    def parse_body(response, include_response = false, format:)
      parsed = case format
               when :json
                 JSON.parse(response.body)
               when :html
                 Nokogiri::HTML(response.body)
               when :csv
                 CSV.parse(response.body)
               else
                 response.body
               end
        if include_response
          return parsed, response
        else
          return parsed
        end
    end

  end
end
