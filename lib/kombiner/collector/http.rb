require 'kombiner/collector'
require 'kombiner/utils/net_utils'

module Kombiner
  class Collector::Http < Kombiner::Collector
    REQUEST_FORMAT = :html
    include Kombiner::Utils::NetUtils

    RECORD_URL_BASE = NotImplementedError.new()
    MIN_ID = 0
    MAX_ID = 1000000 # 1 million to start. Needs override.
    REQUEST_METHOD = 'get'

    def initialize(id_offset: 0, max_id: MAX_ID)
      super()
      @current_id = [MIN_ID, id_offset].max
      @max_id = max_id
      @record_url_base = self.class::RECORD_URL_BASE
    end

    def fetch
      document = nil
      until response_seems_valid?(document)
        return nil if @current_id > @max_id # TODO this should be a raise instead
        document = execute(build_request(get_record_url(record_id: @current_id)))
        @current_id += 1
      end

      document
    end

    def response_seems_valid?(document)
      # maybe should just return true instead? and then override in child
      raise NotImplementedError.new("How am I supposed to know if the response seems valid?")
    end

    def build_request(url, body: nil, headers: nil, method: REQUEST_METHOD, format: nil)
      request =  {
        :method => method,
        :url => url,
        :params => get_request_params(record_id: @current_id),
      }
      request = request.merge(body: body) if body
      request = request.merge(headers: headers) if headers
      request = request.merge(format: format) if format
      # todo logger
      puts "Request: #{request.inspect}"
      request
    end

    def get_request_params(record_id: )
      {} # raise NotImplementedError.new("Get Record URL is not implemented")
    end

    def get_record_url(record_id:)
      raise NotImplementedError.new("Get Record URL is not implemented")
    end

    # def handle_response(response, format: OMG)
    #   super
    # end

  end
end
