require 'kombiner/collector'
require 'csv'

class Kombiner::Collector::Csv < Kombiner::Collector
  
  attr_accessor :current_record, :end_record
  
  def initialize(file, csv_options, options = {})
    super()
    csv_options ||= Hash.new
    @csv = CSV.new(file, csv_options)
    @current_record = options[:offset] ? options[:offset] : 0
    @end_record = options[:limit] + @current_record if options[:limit]
    advance_to_start_row
  end

  def records
    @csv.readlines.size
  end
  
  def fetch
    raise Kombiner::FinishedProcessingError if @current_record >= @end_record
    @current_record += 1
    return @csv.shift.to_hash
  end
  
  private
  
  attr_accessor :csv
  
  def advance_to_start_row
    unless @current_record == 0
      (0..@current_record.to_i).each {@csv.shift}
    end
  end

end
