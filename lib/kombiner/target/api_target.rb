require 'kombiner/data_target'
require 'kombiner/utils/net_utils'
# Abstract Class

class Kombiner::Target::APITarget < Kombiner::Target
  REQUEST_FORMAT = :json
  include Kombiner::Utils::NetUtils
  
  def submit(record)
    raise NotImplementedError
  end

end
