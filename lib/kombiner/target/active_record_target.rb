require 'active_record'

# Abstract Class

class ActiveRecordTarget < Kombiner::Target
  extend Kombiner::DataTarget
   
  def initialize(connection_parameters)
    ActiveRecord::Base.establish_connection(connection_parameters)
  end
  
  def submit(record)
    # Must implement this abstract class.
    raise NotImplementedError
  end
  
end