module Kombiner
  class Collector
    ## Instance Variables
    
    ## does the data source have a header row? If so we will not extract a record from the first row
    @header_row = false
    
    def initialize
      "Initializing new datasource of type: #{self.class}"
      @count = @failures = @successes = 0
    end
    
    def self.portray
      "I AM THE DATAS"
    end
     
    def fetch
      raise NotImplementedError
    end
    
  end

  class DataSourceError < StandardError
    
  end
  
  class FinishedProcessingError < StandardError
    
  end

end

require_relative 'collector/mysql'
require_relative 'collector/http'
require_relative 'collector/csv'
