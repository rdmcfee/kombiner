module Kombiner::Mixin::Parsing

  def parse_date(date, allow_null: false, should_rescue: false, format: nil)
    if allow_null && date.blank?
      return nil
    end
    begin
      if format
        Date.strptime(date, format)
      else
        Date.parse(date)
      end
    rescue Exception => e
      raise e unless should_rescue
      nil
    end
  end

  def parse_price(price, allow_null: false, should_rescue: false)
    return nil if price.blank?
    begin
      return price.gsub("$", "").gsub(",", "").to_f
    rescue Exception => e
      raise e unless should_rescue
      nil
    end
  end

  def parse_integer(int_string, allow_null: false)
    if int_string.blank? && allow_null
      return nil
    end
    raise "Can't convert nil to int" if int_string.blank?
    int_string.to_i
  end

  def parse_float(float_string, allow_null: false)
    if float_string.blank? && allow_null
      return nil
    end
    raise "Can't convert nil to float" if float_string.blank?
    float_string.gsub(/\D/, '').to_f
  end

  def parse_int(int_string, allow_null: false)
    if int_string.blank? && allow_null
      return nil
    end
    raise "Can't convert nil to int" if int_string.blank?
    int_string.gsub(",", "").to_i
  end
end
