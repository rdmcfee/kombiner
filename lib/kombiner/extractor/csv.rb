module Kombiner
  class Extractor::Csv < Kombiner::Extractor
    attr_accessor :columns

    COLUMNS = []

    def initialize(columns: self.class::COLUMNS)
      @columns = columns
      raise "please specify the expected columns" if columns.empty?
    end

    def extract(row)
      if @columns.size != row.size
        raise "The header columns don't seem to line up" 
      end
      if row.is_a? Array
        # could be just array of values
        # produce a hash in format {column: value}
        @columns.zip(row).to_h
      else
        # of could be in hash
        # in which case it's ready to go!
        row
      end
    end

  end
end
