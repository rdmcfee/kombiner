module Kombiner
  class Extractor
    class ParseError < StandardError; end

    def extract(pages)
      result = {}
      pages.each do |page_name, document|
        begin  
          self.send("parse_#{page_name}", document, result)
        rescue Exception => e
          raise ParseError.new("Error parsing #{page_name.to_s}: #{e.inspect}")
        end
      end
      result
    end

  end
end
