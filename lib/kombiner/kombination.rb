## @author Robin McFee
#
#  Kobination wraps a data source and data target with a translation.
#  

module Kombiner
  class Kombination
    attr_reader :source, :target, :mapper, :fetched, :submitted, :created, :updated, :logger, :extractor

    class ArrayTarget
      attr_accessor :res
      def initialize
        @res = []
      end

      def submit(record)
        @res << record
      end
    end

    def initialize(source:, extractor: nil, mapper: nil, target: nil, logger:)
      @source = source
      @extractor = extractor
      @target = target || ArrayTarget.new
      @mapper = mapper
      @extractor = extractor
      @fetched = @submitted = @created = @updated = 0
      @logger = logger
    end

    ## runs the kombination
    #   takes arguements for where to start in the data source, where to finish, and whether to throttle

    def run(options = {})
      begin
        while (record = @source.fetch)
          tmp = "Processing record #{@source.current_record}. #{@fetched} fetched, #{@submitted} submitted, #{@created} created, #{@updated} updated, #{@failed} failed"
          logger.info tmp
          puts tmp
          @fetched += 1
          begin
            # extract the record
            # if @extractor
              record = @extractor.extract(record)
            # end
            # format the record
            if @mapper
              record = @mapper.transform(record)
            end
            # send to data target
            @submitted += 1
            if @target
              if @target.has_record?(record)
                @target.update(record)
                @updated += 1
                logger.info "---- updated"; puts "---- updated"
              else
                @target.create(record)
                @created += 1
                logger.info "---- created"; puts "---- created"
              end
            end
          rescue Kombiner::InvalidRecordError, Kombiner::Utils::NetUtils::RejectionError
            logger.info "---- failed"; puts "---- failed" 
            # This record was not valid
          end
          # ----------- check limits ----------------------------
          if options[:fetch_limit] && (@fetched >= options[:fetch_limit])
            raise Kombiner::FinishedProcessingError.new("Reached fetched limit of #{@fetched} records.")
          end

          # throttle by waiting a specified amount of time before fetching the next record.
          sleep(options[:throttle]) if options[:throttle]
          # add a random factor to the throttling if desired.
          sleep(rand(0.0..options[:rand_throttle])) if options[:rand_throttle]
        end
      rescue Kombiner::FinishedProcessingError
        logger.info "Limit reached at record #{@source.current_record}"
      rescue SystemExit, Interrupt
        logger.info "Program exited by user"
      ensure
        logger.info "Processing finished."
      end
    end
    
    private

    attr_writer :source, :target
  end
end
