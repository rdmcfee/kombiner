## 
# Data target - Abstract Class
# This class defines a link to a target DB or data storage to which we're trying to insert data
# This should be an active_record model?
# Use before_save to GeoCode if GeoCoding is needed

module Kombiner
  class Target
    
    def initialize
      "Initializing new data target of type: #{self.class}"
    end
    
    def submit(record)
      # Implement in the relevant subclass.
      # Basically this is going to do before_save validations (geocoding) and then attempt to save the object.
      raise NotImplementedError
    end
    
  end
  
  class RejectionError < StandardError
    
  end
end
