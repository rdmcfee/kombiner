## Mapper parent class.
#  The user must subclass mapper and implement several basic functions
#  The mapper must accept a hash from the datasource, validate the data, 
#  Mapper can do any validations which don't require comparing to the data target (filter out by time, missing data)
#  Mapper then returns cleaned object


module Kombiner
  class Mapper
    def format(unformatted_record)
      # The user will create a custom subclass of this for their app which will implement this method.
      raise NotImplementedError
    end
  end
end
