require 'kombiner'
require 'kombiner/data_source'
require 'kombiner/data_sources/mysql_source'

describe Kombiner::DataSource do
  it "is a data source" do
    Kombiner::DataSource.portray.should eql("I AM THE DATAS")
  end
  
  it "is a mysql data source" do
    MySQLSource.new.class.name.should eql("MySQLSource")
  end
end

describe Kombiner::DataTarget do
  it "is a data target" do
    Kombiner::DataTarget.portray.should eql("I AM TARGET!")
  end
end

describe Kombiner::Mixin::Formatting::Geocoder do
  it "is a geocoder mixin" do
    Kombiner::Mixin::Formatting::Geocoder.portray.should eql("Geocoder Mixin")
  end
end