require 'kombiner/data_sources/csv_source'

describe Kombiner::DataSource::CSVSource do
  
  let(:data) { "Line No,Description\r\n1,first line\r\n2,second line"}
  let(:source) { Kombiner::DataSource::CSVSource.new(StringIO.new(data), {:headers => :first_row}, {})}
  let(:result) { {"Line No" => "1", "Description" => "first line"} }
  
  it 'should have access to 2 records' do
    source.records.should eql(2)
  end
  
  it 'should be equal to' do
    source.fetch.should eql(result)
  end
  
end