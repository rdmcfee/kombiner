require 'kombiner'
require 'kombiner/mixin'

describe Kombiner::Mixin do
  it "is what it says" do
    Kombiner::Mixin.portray.should eql("Modular Functionality")
  end
end