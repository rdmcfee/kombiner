# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'kombiner/version'

Gem::Specification.new do |spec|
  spec.name          = "kombiner"
  spec.version       = Kombiner::VERSION
  spec.authors       = ["Robin McFee"]
  spec.email         = ["robin.mcfee@gmail.com"]
  spec.description   = %q{This gem will assist in importing data from a third party source to a functional database}
  spec.summary       = %q{Kombiner facilitates pulling data from a data source, formatting and validating the data and importing it into a functional database}
  spec.homepage      = "http://www.clinicbook.io"
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
  spec.add_runtime_dependency 'httparty'
  spec.add_runtime_dependency 'mysql2'
  spec.add_runtime_dependency 'activerecord'
  spec.add_runtime_dependency 'nokogiri'
  spec.add_runtime_dependency 'curb'
  spec.add_runtime_dependency 'typhoeus'
  spec.add_runtime_dependency 'geocoder'

end
